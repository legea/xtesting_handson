# xtesting_handson

The goal of this handson is to explain how to integrate automated tests with
xtesting framework.

Xtesting in a nutshell:

* deploy anywhere plug-and-play CI/CD toolchains in a few commands
* offer all upstream Xtesting-based testcases (e.g.Functest) to the endusers
* support multiple deployment models (all-in-one, centralized services, etc.)
* allow smooth migrations from the tester point of view (from Jenkins to
  GitlabCI via 3 boolean values in a yaml file)

In this handson, Gitlab-ci is used as xtesting-ci tool.

Differents steps needs to be realized to integrate test suite with xtesting.

* Step 1: Optionally, implement some basic scripts to launch test suite with
  xtesting
* Step 2: Build file testcase.yml
* Step 3: Build automatically the docker container from a docker file

Fork the current project before doing the different exercises.

To know how to fork a gitlab project:
<https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html#creating-a-fork>

## Ex1: Integration of tests based on bash scripts

A simple test use case is considered within this exercise. The test is based on
a command line that returns `exit 0` to indicate a successfull execution of the
script:

```shell
echo -n Bash test; exit 0
```

### Tests definition in the file testcase.yml

Look at the file `bash_test/testcases.yaml` to discover how to define a test
based on a shell script.

Each testcase is defined with the following parameters

* name: Name of the test suite
* case_name: use case name
* project_name: project name including this test case
* criteria: A value `100` means that 100% of the tests included in this test
  case shall be successfull to consider the global test OK
* blocking: A failure during this test stops the execution of the test suite
* run:
  * name: Define the xtesting feature on which the test will rely
  * args: arguments of the test

### Build the docker container

#### Docker file definition

The dockerfile is build relying on opnfv/xtesting docker container image.

And for the bash script, it is only requested to copy inside the file `testcase.yaml`.

```dockerfile
FROM opnfv/xtesting

COPY testcases.yaml /usr/lib/python3.8/site-packages/xtesting/ci/testcases.yaml
```

### CI definition in the file gitlab-ci to build the docker container

To build the docker image, a specific runner with priviledged rights relying on
docker-in-docker(dind) docker image is used.

See <https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#use-docker-in-docker-workflow-with-docker-executor>
to get more information.

On the internal gitlab, these runners with priviledged rights are selected with
tags rsc and docker_priviledged

This handson will be executed on public gitlab. No proxy shall be managed.

2 main CI steps are defined to build the docker:

* Login to the git registry with the following command using default git variables:

```shell
docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
```

* Building of the image that is pushed just after in the registry

```shell
cd handson_test
docker build --pull -t "$CI_REGISTRY_IMAGE/handson_test" -f ./docker/Dockerfile .
docker push "$CI_REGISTRY_IMAGE/handson_test"
```

Look at the file .gitlab-ci.yml and at the stages `docker`

### Trigger the generation of bash_test container (if not done)

As you have forked the project, the CI pipeline has not been launched
automatically. To trigger it, a change needs to be pushed within the git
repository.

Add a comment for example in the file .gitlab-ci.yml to trigger the building of
bash_test docker container

Wait some minutes and check the creation of the bash_test container looking at:

* Pipeline execution
* Container Registry

### Run the created docker container from your linux environment

You can test the created container running the docker container from your linux
environment

```shell
docker login -u <your login> registry.gitlab.com
# You have to enter your password to login
docker run --rm registry.gitlab.com/<your login>/xtesting_handson/bash_test:latest
```

### Launch the docker in gitlab-ci

Create a new stage in the file gitlab-ci.yml to launch the new xtesting module handson_test

* Create the new stage `test`
* Use the same runner as for the stage `docker`
* A login to the registry is required
* Run the xtesting module and display the different logs
  docker run -v $PWD:/var/lib/xtesting/results "$CI_REGISTRY_IMAGE/handson_test:latest"

```yaml
run-bash_test:
  stage: test
  image: docker:latest
  services:
    - name: docker:dind
      alias: docker
  before_script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
  script:
    - docker run -v $PWD:/var/lib/xtesting/results "$CI_REGISTRY_IMAGE/bash_test:latest"
    - ls -al
  only:
    - web
```

## Ex2-xtesting integration of python scripts

### Initial situation

Let's take a simple example of a python script where a suite of 2 simple tests
will be executed.
These tests are based on the shell scripts `python_test1.sh` and
`python_test2.sh`.

These scripts will be launched by xtesting calling a python package

### Define the python class to launch the scripts

The python class relies on the xtesting.core python package and especially on
testcase class.

The following python class will be used and enriched

```python
#!/usr/bin/env python
"""TestName test case."""
import logging
import time

import onap_tests.scenario.e2e as e2e
from xtesting.core import testcase


class TestName(testcase.TestCase):
    """To launch TestName."""

    __logger = logging.getLogger(__name__)

    def __init__(self, **kwargs):
        """Init TestName."""
        if "case_name" not in kwargs:
            kwargs["case_name"] = 'Test1'
        super(TestName, self).__init__(**kwargs)
        self.__logger.debug("TestName init started")
        self.test = <name of python script to launch>
        self.start_time = None
        self.stop_time = None
        self.result = 0

    def run(self):
        """Run TestName"""
        self.start_time = time.time()
        self.__logger.debug("start time")
        try:
            # Command to execute the test, to adapt
            self.test.execute()
            self.__logger.info("TestName successfully created")

            # Command to clean the test if any
            self.test.clean()
            self.__logger.info("TestName successfully cleaned")

            # Without exception, return a successfull result, to adapt
            self.result = 100
            self.stop_time = time.time()
            return testcase.TestCase.EX_OK

        except Exception:  # pylint: disable=broad-except
            self.result = 0
            self.stop_time = time.time()
            self.__logger.error("TestName test failed.")
            return testcase.TestCase.EX_TESTCASE_FAILED

    def clean(self):
        """Clean test if relevant."""
        pass
```

Look at the python class `python_test/python_pkg/pythontest.py` that has been
written based on the above python class.

The empty class __init__.py is also required to build the python package.

### Build the python package

The python library setuptools is used to build the python package.

The file setup.cfg needs to be written to reference the different tests of the
python package.

* metadata.name and files.packages are defined with the directory name of the
  python package
* The tests are defined in entry_points section to refer to the right class

Look at the file setup.cfg in the root directory xtesting_handson to understand
how the target python test file is addressed

```Properties
[metadata]
name = python_test
version = 1

[files]
packages = python_test

[entry_points]
xtesting.testcase =
    test1 = python_test.python_pkg.pythontest:Test1
    test2 = python_test.python_pkg.pythontest:Test2
```

### Manage Specific add-ons in the docker file

Look at the file `python_test\docker\Dockerfile`:

```yaml
FROM opnfv/xtesting:jerma

ARG BRANCH=stable/jerma
ARG OPENSTACK_TAG=stable/train

LABEL Thierry Hardy <thierry.hardy@orange.com>

COPY python_test/scripts/python_test1.sh /python_test1.sh
COPY python_test/scripts/python_test2.sh /python_test2.sh
ADD . /src/python_test

RUN set -x && \
    chmod +x /python*.sh && \
    pip3 install \
        -c https://opendev.org/openstack/requirements/raw/branch/\
        $OPENSTACK_TAG/upper-constraints.txt \
        -c https://git.opnfv.org/functest/plain/upper-constraints.txt?h=$BRANCH \
        /src/python_test && \
    rm -r /src/python_test
COPY python_test/docker/testcases.yaml /usr/lib/python3.7/site-packages/xtesting/ci/testcases.yaml
CMD ["run_tests", "-t", "all"]
```

* The shell scripts are copied in the docker image
* The current directory `xtesting_handson` is added
* The python package is installed in the docker image based on current
  directory. The files `setup.cfg` and `setup.py needs to be in the same
directory than`.git` directory to manage versions

An access token is requested to access the gitlab registry.

Update `.gitlab-ci.yml` file to build the container image corresponding to
`python_test/docker/Dockerfile`

### Run the created docker image

Create a new stage in the file `.gitlab-ci.yml` to launch the new xtesting
module python_test

## Ex3: Integration of Robot framework test

Repeat the tasks of exercice 1 to launch a Robot framework test:

* To build the docker image
* To run this docker image

All the artifacts for this exercice are under the directory `robot_test`.
